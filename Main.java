
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;



class Main{
    public static void main(String[] args)  {
    	
    	
    	
    	//Giving input
    	Scanner sc =new Scanner(System.in);
    	System.out.println("Weather forecast of hardcoded 5 cities for consecutive days are :- ");
    	
    	String[] cityNames= {"London","Mumbai","Nagpur","Pune","Paris"};
    	
    	  System.out.println("Please Wait for a while we are fetching weather data......");
    	  System.out.println();
    	  
    	  
    	  
    	//Loop for fetching data for above cities
    	  
    	for(int j=0;j<cityNames.length;j++)
    	{
    	
    	String name=cityNames[j];

        try{
        	
        	
       URL url= new URL("https://api.openweathermap.org/data/2.5/forecast?q="+name+"&appid=1f578388b7ad381b28670eb6a4534471");
       
     //Opening HTTP connection
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();

        conn.setRequestMethod("GET");
        conn.connect();
      //Getting the response code
        int responsecode = conn.getResponseCode();  
        if (responsecode != 200) {
            throw new RuntimeException("HttpResponseCode: " + responsecode);
        } else {

       //Reading Input 
            String inline = "";
            Scanner scanner = new Scanner(url.openStream());
            while (scanner.hasNext()) {
                inline += scanner.nextLine();
            }
         
            scanner.close();
            
            
            JSONParser parse = new JSONParser();
            JSONObject jobj = (JSONObject) parse.parse(inline);//Getting json object
            
        //Parsing json object to get desired data.
            JSONObject city =(JSONObject)jobj.get("city");
            
            System.out.println("Presenting forecast for the city ");
                   
            System.out.println( city.get("name"));
          
            JSONArray jsonarr_1 = (JSONArray) jobj.get("list");
          
            JSONObject firstDate = (JSONObject)jsonarr_1.get(0);
                    
            
            int val=extractDate(firstDate.get("dt_txt").toString());
            
                    
          for(int i=0;i<jsonarr_1.size();i++) 	 

        	  {

        	  //Store the JSON objects in an array
        	 
        	  //Get the index of the JSON object and print the values as per the index

        	  JSONObject jsonobj_1 = (JSONObject)jsonarr_1.get(i);
        	  
        	  if(val==(extractDate(jsonobj_1.get("dt_txt").toString())))
        	  {
        		  	
        		  String dateRaw=jsonobj_1.get("dt_txt").toString();
        		 	
        	    	String date[] =dateRaw.split(" ");
        		  System.out.print("The temp forcast for the day  --> " + date[0]+ " is ");
        		  JSONObject main =(JSONObject)jsonobj_1.get("main");
            	  System.out.println(main.get("temp"));
            	  val++;
        	  }
        	  
        	  }
          
          
          System.out.println("-----------------------------------------------------");

          }
        }
        catch(Exception e)
        {
            System.out.println("Error Enter valid city name and Try again");
        }
      }
    }
    
    
    public  static int extractDate(String date)//Extracting Day 
    {
    	
    	
    	String arr[] =date.split(" ");
    	String[] startDate =arr[0].split("-");
    	
    	return Integer.parseInt(startDate[2]);
    	
    }
    
    
}